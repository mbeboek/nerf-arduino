#pragma once

/**
* @copyright 2018 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

class App
{
    private:
        SerialHandler serialHandler;
        DCMotor motorLeft = DCMotor(3, 7, 4);
        DCMotor motorRight = DCMotor(6, 8, 5);
        NerfTrigger nerfTrigger = NerfTrigger(2);

    public:
        App();

        void runtime();
};