/**
* @copyright 2018 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#pragma once

#include <Arduino.h>
#include <Servo.h>

#include "dc_motor.h"
#include "nerf_trigger.h"
#include "serial_handler.h"

#include "app.h"