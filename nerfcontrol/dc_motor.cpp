/**
* @copyright 2018 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "main_header.h"

DCMotor::DCMotor(int pinSpeed, int pinForwards, int pinBackwards)
: pinSpeed(pinSpeed), pinForwards(pinForwards), pinBackwards(pinBackwards)
{
    pinMode(pinSpeed, OUTPUT);
    pinMode(pinForwards, OUTPUT);
    pinMode(pinBackwards, OUTPUT);

    setSpeed(0);
    stop();
}

void DCMotor::stop()
{
    digitalWrite(pinForwards, LOW);
    digitalWrite(pinBackwards, LOW);
}

void DCMotor::moveInDirection(DCMotorDirection direction)
{
    if(this->direction == direction)
        return;

    this->direction = direction;
    stop();

    if(direction == DCMotorDirection::FORWARDS) {
        digitalWrite(pinForwards, HIGH);
    } else if(direction == DCMotorDirection::BACKWARDS) {
        digitalWrite(pinBackwards, HIGH);
    }
}

void DCMotor::setSpeed(u8 speed)
{
    this->speed = speed;
    analogWrite(pinSpeed, speed);
}
