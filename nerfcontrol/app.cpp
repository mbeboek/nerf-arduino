/**
* @copyright 2018 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "main_header.h"

App::App() {}

void App::runtime()
{
    while(serialHandler.hasData())
    {
        auto action = serialHandler.getAction();
        switch(action.type)
        {
            case SerialActionType::CONTROLS:
                motorLeft.moveInDirection(action.motorLeftDir);
                motorRight.moveInDirection(action.motorRightDir);
                
                if(action.triggerActive) {
                    nerfTrigger.shoot();
                } else {
                    nerfTrigger.defaultPos();
                }
            break;
            case SerialActionType::SPEED_LEFT:
                motorLeft.setSpeed(action.speed);
            break;
            case SerialActionType::SPEED_RIGHT:
                motorRight.setSpeed(action.speed);
            break;

            case SerialActionType::NONE: break;
            default:
                Serial.println("Unknown action type!");
            break;
        }
    }

    delay(100);
}
