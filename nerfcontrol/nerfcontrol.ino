/**
* @copyright 2018 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "main_header.h"

void setup() 
{}

void loop() 
{
	App app = App();

	for(;;)
	{
		app.runtime();
	}
}
