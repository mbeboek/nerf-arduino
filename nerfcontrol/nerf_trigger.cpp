/**
* @copyright 2018 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "main_header.h"

#define ANGLE_NEUTRAL 100
#define ANGLE_TRIGGERED 130
#define DURATION_TRIGGERED 200

NerfTrigger::NerfTrigger(int pin)
: pin(pin)
{
    servo.attach(pin);
    servo.write(ANGLE_NEUTRAL);
    delay(2000);
    isInDefault = true;
}

void NerfTrigger::shoot()
{
    if(isInDefault)
    {
      isInDefault = false;
      servo.write(ANGLE_TRIGGERED);
      delay(DURATION_TRIGGERED);
      servo.write(ANGLE_NEUTRAL);
    }
}

void NerfTrigger::defaultPos()
{
    if(!isInDefault)
    {
      servo.write(ANGLE_NEUTRAL);
      isInDefault = true;
    }
}
