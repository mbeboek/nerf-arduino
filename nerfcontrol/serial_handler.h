#pragma once

/**
* @copyright 2018 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#define SERIAL_BAUD_RATE 9600
#define SERIAL_HANDSHAKE 0xCC

enum class SerialActionType
{
    NONE,
    CONTROLS,
    SPEED_LEFT,
    SPEED_RIGHT
};

struct SerialAction
{
    SerialActionType type;

    DCMotorDirection motorLeftDir;
    DCMotorDirection motorRightDir;
    bool triggerActive;
    
    int speed;
};

class SerialHandler
{
    private:
        DCMotorDirection parseMotorDirection(u8 &data, int bitEnable, int bitDirection);

    public:
        SerialHandler();

        bool hasData();
        SerialAction getAction();
};
