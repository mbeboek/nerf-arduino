#pragma once

/**
* @copyright 2018 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

class NerfTrigger
{
    private:
        Servo servo;
        int pin;
        bool isInDefault = false;

    public:
        NerfTrigger(int pin);
        void shoot();
        void defaultPos();
};
