
/**
* @copyright 2018 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include "main_header.h"

#define BIT_IS_SPEED    0
#define BIT_WHICH_SPEED 1

#define BIT_MOTOR_L     1
#define BIT_MOTOR_L_DIR 2
#define BIT_MOTOR_R     3
#define BIT_MOTOR_R_DIR 4
#define BIT_TRIGGER     5

SerialHandler::SerialHandler()
{
    Serial.begin(SERIAL_BAUD_RATE);
	  Serial.write(SERIAL_HANDSHAKE);
}

DCMotorDirection SerialHandler::parseMotorDirection(u8 &data, int bitEnable, int bitDirection)
{
    if(data & (1 << bitEnable)) 
    {
        return (data & (1 << bitDirection)) 
            ? DCMotorDirection::FORWARDS 
            : DCMotorDirection::BACKWARDS;
    }
    return DCMotorDirection::STOPPED;
}

bool SerialHandler::hasData()
{
    return Serial.available() > 0;
}

SerialAction SerialHandler::getAction()
{
    SerialAction action;
    action.type = SerialActionType::NONE;

    if(hasData()) 
    {
        u8 data = Serial.read();

        if(data & (1 << BIT_IS_SPEED)) 
        {
            action.type = (data & (1 << BIT_WHICH_SPEED)) 
                ? SerialActionType::SPEED_LEFT 
                : SerialActionType::SPEED_RIGHT;
            
            action.speed = ((data >> 2) * 2) + 129;
            return action;
        }

        action.type = SerialActionType::CONTROLS;
        action.motorLeftDir = parseMotorDirection(data, BIT_MOTOR_L, BIT_MOTOR_L_DIR);
        action.motorRightDir = parseMotorDirection(data, BIT_MOTOR_R, BIT_MOTOR_R_DIR);
        action.triggerActive = data & (1 << BIT_TRIGGER);
    }

    return action;
}
